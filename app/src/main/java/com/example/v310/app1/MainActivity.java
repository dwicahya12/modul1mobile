package com.example.v310.app1;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText Edtalas, Edttinggi;
    private Button btnCek;
    private TextView hasil;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Edtalas = (EditText)findViewById(R.id.alas);
        Edttinggi = (EditText)findViewById(R.id.tinggi);
        btnCek = (Button)findViewById(R.id.button);
        hasil = (TextView)findViewById(R.id.text1);

        btnCek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String alas,tinggi;
                alas = Edtalas.getText().toString();
                tinggi = Edttinggi.getText().toString();

                if (TextUtils.isEmpty(alas)){
                    Edtalas.setError("Tidak Boleh Kosong!");
                    Edtalas.requestFocus();
                } else if (TextUtils.isEmpty(tinggi)){
                    Edttinggi.setError("Tidak Boleh Kosong!");
                    Edttinggi.requestFocus();
                } else {
                    int a = Integer.parseInt(alas);
                    int t = Integer.parseInt(tinggi);
                    int luas = a*t;

                    hasil.setText(Integer.toString(luas));
                }

            }
        });

    }
}
